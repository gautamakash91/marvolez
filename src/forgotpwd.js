import React, {Component} from 'react';
import {
    Link,
  } from 'react-router-dom';
import axios from 'axios';
import {connect} from 'react-redux';
import {Row, Col, Card, Form, Input, Button, Select,AutoComplete } from 'antd';
import image from './images/stars.jpeg'
import ForgotpwdFormComp from './forgotpwdForm';
import LoginFormComp from './LoginForm';
import Head from './header';

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;


  
class Forgotpwd extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email:''
        }
    }

    render() {

        return(
            <div style = {{backgroundImage: `url(${image})`, height:730, width: "100%"}}>
                <div>
                    <Head />
                </div>
         
            <Row>
                <Col 
                xs={{ span: 24 }} sm={{ span: 14, offset: 5 }}>
                <Card 
                title="Forgot your password? Enter your email to retrieve your account!" style={{marginTop: 160 }}>
                    <ForgotpwdFormComp />
                </Card>
                </Col>
            </Row>
            </div>
        );
    }
}


function mapStateToProps(state){
    return{
      user: state.reducerUser,
    };
  }
  
export default connect(mapStateToProps)(Forgotpwd);