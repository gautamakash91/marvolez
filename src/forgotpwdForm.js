import React, {Component} from 'react';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
var axios = require('axios');


class ForgotpwdForm extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    message:false,
    loading:false,
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);

        this.setState({
            loading:true
        });

        axios.post("https://api.marvolez.com/reset",{
            email: values.email,
        })
        .then((response) => {
    
        if (response.data===true) {
            this.setState({
                message:'Registration Successful. Please login.',
            });
        }
        else this.setState({message: Error, loading:false });
        }).catch(err => {
        this.setState({ message: err.message });
        });
      }
    });
  }
  
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;
    let msg = null;
    if (this.state.message===false) {
        msg = <div></div>;
      } else {
        msg = <center style={{backgroundColor:'#f2f2f2', padding:10, marginBottom:10}}>{this.state.message}</center>;
      }

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    const websiteOptions = autoCompleteResult.map(website => (
      <AutoCompleteOption key={website}>{website}</AutoCompleteOption>
    ));

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem
          {...formItemLayout}
          label="E-mail"
        >
          {getFieldDecorator('email', {
            rules: [{
              type: 'email', message: 'The input is not valid E-mail!',
            }, {
              required: true, message: 'Please input your E-mail!',
            }],
          })(
            <Input />
          )}
        </FormItem>

        <FormItem {...tailFormItemLayout}>
          <Button 
          type="primary" 
          loading={this.state.loading}
          htmlType="submit">Retrieve</Button>
        </FormItem>
      </Form>
    );
  }
}

const ForgotpwdFormComp = Form.create()(ForgotpwdForm);
  
export default ForgotpwdFormComp;