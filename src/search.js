import React, {Component} from 'react';
import { Menu, Card, Button, Row, Col, Icon, Input, Layout, LocaleProvider, Tooltip } from 'antd';
import axios from 'axios';
import Head from './header';
import {connect} from 'react-redux';
import {
    Redirect
  } from 'react-router-dom';

import enUS from 'antd/lib/locale-provider/en_US';
import ConfirmCode from './confirmcode';
import image from './images/search.jpg';
import AddRestaurant from './addRestaurant/AddRestaurant';
import logo from './images/marvolez.png';

const { Content } = Layout;


class Search extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search:'',
            data:[],
            name: "",
            city: "",
            code:"",
            pincode: "",
            dashboard:false,
            logout:false
        }
    }

    search = () => {
        
        var self = this;

        axios.post("https://api.marvolez.com/getByInfo",{
            name: this.state.name,
            pincode: this.state.pincode,
            code:this.state.code
        })
        .then(function(response){
            console.log(response.data);
            self.setState({
                data: response.data
            });
        });
        
    }
        


    handleClick = (e) => {
        console.log('click ', e);
        if(e.key==='search'){
            this.setState({
                search:true
            });
        }else if(e.key==='logout'){
            this.setState({
                logout:true
            });
        }else if(e.key==='dashboard'){
            this.setState({
                dashboard:true
            });
        }else if(e.key==='settings'){
            this.setState({
                settings:true
            });
        }
      }

    render() {
        if (this.state.search) {
            return <Redirect to='/search'/>;
        } else if (this.state.logout) {
            return <Redirect to='/logout'/>;
        }else if (this.state.dashboard) {
            return <Redirect to='/dashboard'/>;
        }else if (this.state.settings) {
            return <Redirect to='/settings'/>;
        }
        
        return(

            <LocaleProvider locale={enUS}>

                <Layout>
                <Row style={{height:60}}>
                        <img style={{marginLeft:'49%'}} src={logo} alt="logo" height="55px" width="55px" />
                    </Row>

                    <Layout style={{marginTop: 25}}>
                        <Row>
                            <Col
                            xs={{ span: 24 }} lg={{ span: 6 }} sm={{ span: 6}} md={{ span: 6}} 
                            style={{ background: '#fff' }}>

                            <Menu
                            onClick={this.handleClick}
                            defaultSelectedKeys={['search']}
                            defaultOpenKeys={['']}
                            mode="inline"
                            theme="dark"
                            >
                                <Menu.Item key="dashboard">Dashboard</Menu.Item>
                                <Menu.Item key="search">Search</Menu.Item>
                                <Menu.Item key="settings">Settings</Menu.Item>
                                <Menu.Item key="logout">Logout</Menu.Item>
                                
                            </Menu>
                            <center>
                            <Tooltip title="Add your restaurant's details and we will verify it for you. Once the restaurant is verified you will be notified about the update. " >
                                <Icon style={{fontSize:15, marginTop:20,marginBottom:10}} type="question-circle-o" />
                            </Tooltip>
                            <p style={{fontSize:15 , marginBottom:10}}>If you don't find your restaurant</p>
                            
                            
                            <AddRestaurant />
                            </center>
                            </Col>
                            <Col 
                            xs={{ span: 24 }} lg={{ span: 18 }} sm={{ span: 18 }} md={{ span: 18 }} 
                            >
                            

                            <Layout style={{ padding: '0 12px 12px' }}>
                                <Content>
                                <Row style={{paddingTop: 100, paddingBottom: 100, backgroundImage: 'url(' +image+ ')',backgroundSize: '100%',}}>
                                <Col className="gutter-row" span={6} offset={6}>
                                    <div className="gutter-box">
                                        <Input prefix={<Icon type="search" style={{ fontSize: 13 }} />} placeholder="Search by Name" onChange={(value) => this.setState({name: value.target.value})} />
                                    </div>
                                </Col>
                                <Col className="gutter-row" span={6} offset={1}>
                                    <div className="gutter-box">
                                        <Input prefix={<Icon type="search" style={{ fontSize: 13 }} />} placeholder="Search by ZipCode" onChange={(value) => this.setState({pincode: value.target.value})} />
                                    </div>
                                </Col>
                                <Col className="gutter-row" span={3}>
                                    <div className="gutter-box">
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            className="login-form-button"
                                            onClick={this.search.bind(this)}   
                                            style={{marginLeft: 10}} 
                                        >
                                            Search
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                                <div style={{ background: '#b2dfdb'}}>
                                    <Card title="RESULTS" bordered={true} style={{ textAlign: 'center' }} >
                                    <Row>
                                        {this.state.data.map((data) => ( 
                                            
                                                <Col xs={{ span: 11, offset: 1 }} sm={{ span: 7, offset: 1 }}>
                                                    <div key={data._id}>
                                                            <Card title={data.Name.toUpperCase()} style={{backgroundColor:'#ffffff' ,textAlign: 'center', height:170}}>
                                                                <p style={{ textAlign: 'center'}}>{data.Address.toUpperCase()}</p>
                                                                <p style={{ textAlign: 'center'}}>{data.Phone}</p>
                                                                <ConfirmCode {...{id:data._id,phone: data.Phone} } />
                                                            </Card>
                                                            <br />
                                                    </div>
                                                </Col>
                                        ))}
                                        </Row>
                                    </Card>                            
                                </div>
                                </Content>
                            </Layout>
                            </Col>
                            </Row>
                </Layout>
                </Layout>
            </LocaleProvider>
        );
    }
}

function mapStateToProps(state){
    return{
      user: state.reducerUser,
    };
  }
  
export default connect(mapStateToProps)(Search);