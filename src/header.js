import React, {Component} from 'react';
import {
    Redirect, Link
  } from 'react-router-dom';
  import {connect} from 'react-redux';
import logo from './images/marvolez.png';
import { Layout,Menu, Row, Col, Button, Icon } from 'antd';


class Head extends Component {

    constructor(props) {
        super(props);
        this.state = {
            current:''
        }
    } 

    handleClick = (e) => {
        console.log('click ', e);
        this.setState({
          current: e.key,
        });
      }

    render() {

        if (this.state.current=='signup') {
            return <Redirect to='/signup'/>;
        }else if (this.state.current=='signin') {
            return <Redirect to='/'/>;
        }

        return(
            
            <div>
                <Row style={{height:60, backgroundColor:'#ffffff'}}>
                    <img style={{marginLeft:'49%'}} src={logo} alt="logo" height="55px" width="55px" />
                </Row>
                <Menu
                    theme="dark"
                    onClick={this.handleClick}
                    selectedKeys={[this.state.current]}
                    mode="horizontal"
                    style={{}}
                >
                
                    <Menu.Item key="signin">
                    <Icon type="lock" />Sign In
                    </Menu.Item>
                    <Menu.Item key="signup">
                    <Icon type="user-add" />Sign Up
                    </Menu.Item>
                </Menu>
            </div>



        );
    }
}



function mapStateToProps(state){
    return{
      user: state.reducerUser,
    };
  }
  
export default connect(mapStateToProps)(Head);
