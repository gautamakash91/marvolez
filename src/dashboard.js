import React, {Component} from 'react';
import AddSpecial from './addspecial';
import Head from './header';
import {connect} from 'react-redux';
import {
    Redirect
  } from 'react-router-dom';
import {Button, Row, Col, Layout, LocaleProvider, Icon, Menu, Card, Switch } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import DeleteSpl from './deleteSpecial';
import logo from './images/marvolez.png';

var axios = require('axios');


class Dashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            restaurants:[],
            specials:[],
            message: '',
            collapsed: false,
            isActive: false,
            search:false,
            logout:false

        }
    }

    componentDidMount  () {
        if (this.props.user.authed) {
            axios.post("https://api.marvolez.com/getByEmail",{
          //axios.post("http://localhost:8000/getByEmail",{
            email:this.props.user.email
        }).then((response)=>{
            console.log("resp: "+response.data);
            this.setState({
                restaurants: response.data
            });
        }).catch(err => {
        });
        } else {
            return <Redirect to='/'/>;
        }
    }

    getRestaurants(){
        axios.post("https://api.marvolez.com/getByEmail",{
            //axios.post("http://localhost:8000/getByEmail",{
                email:this.props.user.email
            }).then((response)=>{
                console.log("resp: "+response.data);
                this.setState({
                    restaurants: response.data
                });
            }).catch(err => {
            });
    }


    handleClick = (e) => {
        console.log('click ', e);
        if(e.key==='search'){
            this.setState({
                search:true
            });
        }else if(e.key==='logout'){
            this.setState({
                logout:true
            });
        }else if(e.key==='dashboard'){
            this.setState({
                dashboard:true
            });
        }else if(e.key==='settings'){
            this.setState({
                settings:true
            });
        }
    }

    render() {
        if (this.state.search) {
            return <Redirect to='/search'/>;
        } else if (this.state.logout) {
            return <Redirect to='/logout'/>;
        }else if (this.state.settings) {
            return <Redirect to='/settings'/>;
        }
 
        return(
            <div>
                <LocaleProvider locale={enUS}>
                    <Layout>
                    <Row style={{height:55}}>
                        <img style={{marginLeft:'49%'}} src={logo} alt="logo" height="55px" width="55px" />
                    </Row>
                        <Layout style={{marginTop: 5}}>
                        <Row>
                            <Col
                            xs={{ span: 24 }} lg={{ span: 6 }} sm={{ span: 6}} md={{ span: 6}} 
                            style={{ background: '#fff' }}>
                                <Menu
                                onClick={this.handleClick}
                                defaultSelectedKeys={['dashboard']}
                                defaultOpenKeys={['']}
                                mode="inline"
                                theme="dark"
                                >
                                    <Menu.Item key="dashboard">Dashboard</Menu.Item>
                                    <Menu.Item key="search">Search</Menu.Item>
                                    <Menu.Item key="settings">Settings</Menu.Item>
                                    <Menu.Item key="logout">Logout</Menu.Item>

                                </Menu>
                            </Col>
                            <Col 
                            xs={{ span: 24 }} lg={{ span: 18 }} sm={{ span: 18 }} md={{ span: 18 }} 
                            > 
                                    <div style={{ background: '#ECECEC' }}>
                                        <Card title="YOUR SPECIALS" bordered={false} style={{ textAlign: 'center' }} 
                                            extra={
                                            <div>
                                            <Button type="primary" 
                                            onClick={this.getRestaurants.bind(this)}>Refresh List</Button>
                                            <AddSpecial {...{restaurants:this.state.restaurants}} /> 
                                            </div>
                                        }
                                        >
                                        <Row>
                                            {this.state.restaurants.map((data) => (
                                                <Col
                                                xs={{ span: 11, offset: 1 }} sm={{ span: 7, offset: 1 }}
                                                key={data._id}
                                                >
                                                    {data.Name.toUpperCase()} - 
                                                    {data.Address.toUpperCase()}
                                                    {data.Specials.map((spl) => (
                                                        <Card 
                                                        title={spl.Title}
                                                        >
                                                            Category: {spl.Type}
                                                            Start Time: {spl.StartTime}
                                                            End Time: {spl.EndTime}
                                                            <DeleteSpl {...{id:data._id, Title: spl.Title} } />
                                                        </Card>
                                                    ))}
                                                </Col>
                                            ))}
                                            </Row>
                                        </Card>
                                    </div>
                                
                            </Col>
                            </Row>
                        </Layout>
                    </Layout>
                </LocaleProvider>
            </div>
        );
    }
}


function mapStateToProps(state){
    return{
      user: state.reducerUser,
    };
  }
  
  function mapDispatchToProps(dispatch){
    return{
      setEmailType:(emailtype) => {
        dispatch({
          type:"LOGGED_IN",
          payload:emailtype
        });
      }
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);