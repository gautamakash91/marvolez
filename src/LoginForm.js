import React, {Component} from 'react';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import {
    Link,
    Redirect
  } from 'react-router-dom';
import {connect} from 'react-redux';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
var axios = require('axios');


class LoginForm extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    message:false,
    loading:false
  };

  handleSubmit = (e) => {
    e.preventDefault();
    var self = this;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);

        this.setState({
            loading:true
        });

        axios.post("https://api.marvolez.com/login",{
        //axios.post("http://localhost:8000/login",{
            email: values.email,
            password:values.password,
        })
        .then((response) => {
            if(response.data[0].valid){
                self.props.setEmailType({email: values.email, type:response.data[0].type});
            }else{
                self.setState({
                  loading:true,
                  message:'Incorrect credentials entered',
                });
            }
        }).catch(err => {
        });
      }
    });
  }
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }
  

  render() {
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    let msg = null;
    if (this.state.message===false) {
        msg = <div></div>;
    } else {
        msg = <center style={{backgroundColor:'#f2f2f2', padding:10, marginBottom:10}}>{this.state.message}</center>;
    }

    if (this.props.user.authed) {
        return <Redirect to='/dashboard'/>;
    }

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem
          {...formItemLayout}
          label="E-mail"
        >
          {getFieldDecorator('email', {
            rules: [{
              type: 'email', message: 'The input is not valid E-mail!',
            }, {
              required: true, message: 'Please input your E-mail!',
            }],
          })(
            <Input />
          )}
        </FormItem>
        
        <FormItem
          {...formItemLayout}
          label="Password"
        >
          {getFieldDecorator('password', {
            rules: [{
              required: true, message: 'Please input your password!',
            }, {
              validator: this.checkConfirm,
            }],
          })(
            <Input type="password" />
          )}
        </FormItem>

        {msg}

        <FormItem {...tailFormItemLayout}>
          <Button 
          type="primary" 
          loading={this.state.loading}
          htmlType="submit"
          style={{width:100}}>
            Login
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const LoginFormComp = Form.create()(LoginForm);


function mapStateToProps(state){
    return{
      user: state.reducerUser,
    };
  }
  
  function mapDispatchToProps(dispatch){
    return{
      setEmailType:(emailtype) => {
        dispatch({
          type:"LOGGED_IN",
          payload:emailtype
        });
      }
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(LoginFormComp);