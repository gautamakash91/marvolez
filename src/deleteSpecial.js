import React, {Component} from 'react';
import axios from 'axios';
import { message, Modal, Button, Row, Col, Input, Form } from 'antd';
import {connect} from 'react-redux';

class DeleteSpl extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  delSpecial  () {
      axios.post("https://api.marvolez.com/deleteSpecial",{
        //axios.post("http://localhost:8000/deleteSpecial",{
        id:this.props.id,
        Title: this.props.Title
    }).then((response)=>{
        console.log("response: "+response.data);
        if(response.data[0].delete){
            message.success('SPECIAL DELETED SUCCESSFULLY');
        }else{
            message.error('UNABLE TO DELETE SPECIAL');
        }
    }).catch(err => {
    });
    }

    render() {
 
        return(
            <Button
            type="primary" 
            onClick={this.delSpecial.bind(this)}
            >
                Delete
            </Button>
        );
    }
}

export default DeleteSpl;