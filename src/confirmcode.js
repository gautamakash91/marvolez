import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button, Row, Col, Input, Form } from 'antd';
import {connect} from 'react-redux';

class ConfirmCode extends Component {

  constructor(props) {
    super(props);
    this.state = {
        usercode:"",
        visible:false,
        code:String(Math.floor(Math.random()*8999+1000)),
        confirmLoading:false
    }
  }


  showModal = () => {
    
    this.setState({
      visible:true
    });
    console.log("state code"+this.state.code);
    axios.post("https://api.marvolez.com/plivo",{
        phone:this.props.phone,
        code:this.state.code,
    })
    .then((response)=>{
        this.setState({
          visible: true,
          specials: response.data
        });
    });
  }

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  }

  verifycode(event) {
    if(this.state.code === this.state.usercode){
      axios.post("https://api.marvolez.com/addOwner",{
        id:this.props.id,
        email:this.props.user.email,
        hexCode:"TiKfGRskuIMB2h7iFOyszpCE3Sxpgp",
      })
      .then(()=>{
        this.setState({
          visible: false,
          confirmLoading: false,
          code:'',
          usercode:'',
          phone:'',
        });
      });
    }
  }

  render() {
    return(
      <div>
          <Button type="primary" onClick={this.showModal} style={{marginTop:15}}>Claim</Button>
          <Modal title="Confirm code"
            visible={this.state.visible}
            onOk={this.verifycode.bind(this)}
            confirmLoading={this.state.confirmLoading}
            onCancel={this.handleCancel}
          >

          <Form>
            <Col gutter={16}>
                <Row className="gutter-row" span={6}>
                  <div className="gutter-box">
                      <Input placeholder="Enter Code" name="usercode" value={this.state.usercode} onChange={(value) => this.setState({usercode: value.target.value})} onSubmit={this.verifycode}/>
                  </div>
                </Row>             
            </Col>
          </Form>
          </Modal>
      </div>
    );
  }
}


function mapStateToProps(state){
  return{
    user: state.reducerUser,
  };
}

export default connect(mapStateToProps)(ConfirmCode);