import React, {Component} from 'react';
import axios from 'axios';
import {message, Modal, Button, Row, Col, Input, Form , Select} from 'antd';
import {connect} from 'react-redux';
import moment from 'moment';

const Option = Select.Option;

class AddSpecial extends Component {

  constructor(props) {
    super(props);
    this.state = {
        RestaurantName:'',
        title: '',
        visible: false,
        confirmLoading: false,
        category:'',
        restaurants:[],
        startTime:'00:00',
        endTime:'00:00',
        message:''
    }
    console.log("current: "+moment().format("HH:mm:ss"));
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  handleOk = () => {

    axios.post("https://api.marvolez.com/addSpecial",{
    //axios.post("http://localhost:8000/addSpecial",{
      id:this.state.id,
      title:this.state.title,
      type:this.state.type,
      startTime: this.state.startTime,
      endTime: this.state.endTime
    })
    .then((response)=>{
      this.setState({
        visible: false,
        confirmLoading: false,
        id:'',
        title: '',
        type: 'daystarters',
        startTime: '',
        endTime:'',
        value:null
      });

      if(response.data[0].add){
        message.success('SPECIAL ADDED SUCCESSFULLY');
    }else{
        message.error('UNABLE TO ADD SPECIAL');
    }
      
    }).catch(function (error) {
      console.log(error);
    });
  }

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  }

  
  onOk(value) {
    console.log('onOk: ', value);
  }

  render() {

    const { visible, confirmLoading } = this.state;

    return(
      <div>
        <div>
          <Button type="primary" onClick={this.showModal.bind(this)}>Add Special</Button>
          <Modal title="Add Special"
            visible={visible}
            onOk={this.handleOk}
            
            confirmLoading={confirmLoading}
            onCancel={this.handleCancel}
          >

          <Form>
            <Col gutter={16}>

            <Row>
              Restaurant: 
              <div className="gutter-box">
                <Select
                    defaultValue="Select"
                    style={{ width: '32%' }}
                    onChange={(value) => this.setState({id: value})}
                >
                  <Option value="Select" disabled>None</Option> 
                  {this.props.restaurants.map((data) => (
                      <Option value={data._id}>{data.Name}</Option>
                  ))}
                </Select>
              </div>
            </Row>

              <br />
              <Row className="gutter-row" span={6}>
                  Specials Detail: 
                  <div className="gutter-box">
                      <Input placeholder="Title" name="title" value={this.state.title} onChange={(value) => this.setState({title: value.target.value})} />
                  </div>
              </Row>

              <br />
              <Row className="gutter-row" span={6}>
                  Category: 
                <Select defaultValue="daystarters" style={{ width: 120 }} onChange={(value) => this.setState({type: value})}>
                  <Option value="daystarters">Day Starters</Option>
                  <Option value="middaymunch">Mid Day Munch</Option>
                  <Option value="chilltime">Chill Time</Option>
                  <Option value="happyhour">Happy Hour</Option>
                </Select>
              </Row>

              <br />

              <Row className="gutter-row" span={6}>
                  Start Time: 
                <Select defaultValue="00:00" style={{ width: 120 }} onChange={(value) => this.setState({startTime: value})}>
                  <Option value="00:00">00:00</Option>
                  <Option value="01:00">01:00</Option>
                  <Option value="02:00">02:00</Option>
                  <Option value="03:00">03:00</Option>
                  <Option value="04:00">04:00</Option>
                  <Option value="05:00">05:00</Option>
                  <Option value="06:00">06:00</Option>
                  <Option value="07:00">07:00</Option>
                  <Option value="08:00">08:00</Option>
                  <Option value="09:00">09:00</Option>
                  <Option value="10:00">10:00</Option>
                  <Option value="11:00">11:00</Option>
                  <Option value="12:00">12:00</Option>
                  <Option value="13:00">13:00</Option>
                  <Option value="14:00">14:00</Option>
                  <Option value="15:00">15:00</Option>
                  <Option value="16:00">16:00</Option>
                  <Option value="17:00">17:00</Option>
                  <Option value="18:00">18:00</Option>
                  <Option value="19:00">19:00</Option>
                  <Option value="20:00">20:00</Option>
                  <Option value="21:00">21:00</Option>
                  <Option value="22:00">22:00</Option>
                  <Option value="23:00">23:00</Option>
                </Select>
              </Row>

              <br />

              <Row className="gutter-row" span={6}>
                  End Time: 
                <Select defaultValue="00:00" style={{ width: 120 }} onChange={(value) => this.setState({endTime: value})}>
                  <Option value="00:00">00:00</Option>
                  <Option value="01:00">01:00</Option>
                  <Option value="02:00">02:00</Option>
                  <Option value="03:00">03:00</Option>
                  <Option value="04:00">04:00</Option>
                  <Option value="05:00">05:00</Option>
                  <Option value="06:00">06:00</Option>
                  <Option value="07:00">07:00</Option>
                  <Option value="08:00">08:00</Option>
                  <Option value="09:00">09:00</Option>
                  <Option value="10:00">10:00</Option>
                  <Option value="11:00">11:00</Option>
                  <Option value="12:00">12:00</Option>
                  <Option value="13:00">13:00</Option>
                  <Option value="14:00">14:00</Option>
                  <Option value="15:00">15:00</Option>
                  <Option value="16:00">16:00</Option>
                  <Option value="17:00">17:00</Option>
                  <Option value="18:00">18:00</Option>
                  <Option value="19:00">19:00</Option>
                  <Option value="20:00">20:00</Option>
                  <Option value="21:00">21:00</Option>
                  <Option value="22:00">22:00</Option>
                  <Option value="23:00">23:00</Option>
                </Select>
              </Row>
            
            </Col>
          </Form>
          </Modal>
        </div>
      </div>
    );
  }
}


function mapStateToProps(state){
  return{
    user: state.reducerUser,
  };
}

function mapDispatchToProps(dispatch){
  return{
    setEmailType:(emailtype) => {
      dispatch({
        type:"LOGGED_IN",
        payload:emailtype
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddSpecial);