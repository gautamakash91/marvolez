import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Routes from './routes';
// import AddRestaurant from './addRestaurant';
// import AddSpecial from './addspecial';

import {createStore} from 'redux';
import reducers from './reducer/index';
import {Provider} from 'react-redux';

const store = createStore(reducers);


ReactDOM.render(
    <Provider store={store}>
        <Routes />
    </Provider>, document.getElementById('root'));

// ReactDOM.render(
//     <AddSpecial />, document.getElementById('root'));