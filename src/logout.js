import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Redirect
  } from 'react-router-dom';

class Logout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        };
    }
    componentWillMount() {
        this.props.logout();
        this.setState({
            redirect:true
        });
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to='/'/>;
        }
    }
}

function mapStateToProps(state){
    return{
        user: state.reducerUser,
    };
}
  
function mapDispatchToProps(dispatch){
    return{
        logout:() => {
            dispatch({
                type:"LOGOUT"
            });
        }
    };
}
  
export default connect(mapStateToProps, mapDispatchToProps)(Logout);

