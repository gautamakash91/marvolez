import React, {Component} from 'react';
import AddSpecial from './addspecial';
import Head from './header';
import {connect} from 'react-redux';
import {
    Redirect
  } from 'react-router-dom';
import {Button, Row, Col, Layout, LocaleProvider, Icon, Menu, Card, Switch } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import DeleteSpl from './deleteSpecial';
import ChangePasswordForm from './ChangePasswordForm';
var axios = require('axios');


class ChangePassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userDetails
        }
    }


    handleClick = (e) => {
        console.log('click ', e);
        if(e.key==='search'){
            this.setState({
                search:true
            });
        }else if(e.key==='logout'){
            this.setState({
                logout:true
            });
        }
    }

    render() {
        if (this.state.search) {
            return <Redirect to='/search'/>;
        } else if (this.state.logout) {
            return <Redirect to='/logout'/>;
        }
 
        return(
            <div>
                <LocaleProvider locale={enUS}>
                    <Layout>

                        <Head />

                        <Layout style={{marginTop: 25}}>
                        <Row>
                            <Col
                            xs={{ span: 24 }} lg={{ span: 6 }} sm={{ span: 6}} md={{ span: 6}} 
                            style={{ background: '#fff' }}>
                                <Menu
                                onClick={this.handleClick}
                                defaultSelectedKeys={['dashboard']}
                                defaultOpenKeys={['']}
                                mode="inline"
                                theme="dark"
                                >
                                    <Menu.Item key="dashboard">Dashboard</Menu.Item>
                                    <Menu.Item key="search">Search</Menu.Item>
                                    <Menu.Item key="logout">Logout</Menu.Item>

                                </Menu>
                            </Col>
                            <Col 
                            xs={{ span: 24 }} lg={{ span: 18 }} sm={{ span: 18 }} md={{ span: 18 }} 
                            > 
                                <ChangePasswordForm />
                            </Col>
                            </Row>
                        </Layout>
                    </Layout>
                </LocaleProvider>
            </div>
        );
    }
}


function mapStateToProps(state){
    return{
      user: state.reducerUser,
    };
  }
  
  function mapDispatchToProps(dispatch){
    return{
      setEmailType:(emailtype) => {
        dispatch({
          type:"LOGGED_IN",
          payload:emailtype
        });
      }
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);