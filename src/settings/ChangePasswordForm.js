import React, {Component} from 'react';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
var axios = require('axios');


class ChangePasswordComp extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    message:false,
    loading:false,
  };

  HandleChangePassword(e){
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);

        this.setState({
            loading:true
        });

        axios.post("https://api.marvolez.com/ChangePassword",{
            email: this.props.user.email,
            password:values.password,
        })
        .then((response) => {
    
        if (response.data===true) {
            this.setState({
                message:'Password Changed',
                loading:false
            });
        }
        else this.setState({message: Error, loading:false });
        }).catch(err => {
        this.setState({ message: err.message });
        });
      }
    });
  }
  
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;
    let msg = null;
    if (this.state.message===false) {
        msg = <div></div>;
      } else {
        msg = <center style={{backgroundColor:'#f2f2f2', padding:10, marginBottom:10}}>{this.state.message}</center>;
      }

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '+1',
    })(
      <Select style={{ width: 70 }}>
        <Option value="+1">+1</Option>
      </Select>
    );

    const websiteOptions = autoCompleteResult.map(website => (
      <AutoCompleteOption key={website}>{website}</AutoCompleteOption>
    ));

    return (
      <Form onSubmit={this.HandleChangePassword}>
        <FormItem
          {...formItemLayout}
          label="Password"
        >
          {getFieldDecorator('password', {
            rules: [{
              required: true, message: 'Please input your password!',
            }, {
              validator: this.checkConfirm,
            }],
          })(
            <Input type="password" />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Confirm Password"
        >
          {getFieldDecorator('confirm', {
            rules: [{
              required: true, message: 'Please confirm your password!',
            }, {
              validator: this.checkPassword,
            }],
          })(
            <Input type="password" onBlur={this.handleConfirmBlur} />
          )}
        </FormItem>
        

        <FormItem {...tailFormItemLayout}>
          <Button 
          type="primary" 
          loading={this.state.loading}
          htmlType="submit">Change Password</Button>
        </FormItem>
      </Form>
    );
  }
}

const ChangePasswordForm = Form.create()(ChangePasswordComp);
  
export default ChangePasswordForm;