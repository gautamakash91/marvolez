import React, {Component} from 'react';
//import ReactDOM from 'react-dom';
import { Menu, Row, Col, Form, Icon, Input, Button, } from 'antd';
import {
    Link,
    Redirect
  } from 'react-router-dom';
  import image from './images/login.jpg';
import Head from './header';
import LoginFormComp from './LoginForm';
import logo from './images/marvolez.png';

//import Navbar from './navbar';
//import { Route, BrowserRouter, Link, Redirect, Switch } from 'react-router-dom';

// var provider = new firebase.auth.FacebookAuthProvider();

// var provider2 = new firebase.auth.GoogleAuthProvider();

class Login extends Component { 

constructor(props) {
    super(props);
    this.state = {
        current:'signin'
    }
}

// facebook() {
//     firebase.auth().signInWithPopup(provider).then(function(result) {
//         // This gives you a Facebook Access Token. You can use it to access the Facebook API.
//         var token = result.credential.accessToken;
//         // The signed-in user info.
//         var user = result.user;
//         // ...
//       }).catch(function(error) {
//         // Handle Errors here.
//         var errorCode = error.code;
//         var errorMessage = error.message;
//         // The email of the user's account used.
//         var email = error.email;
//         // The firebase.auth.AuthCredential type that was used.
//         var credential = error.credential;
//         // ...
//       });
// }

// google() {
//     firebase.auth().signInWithPopup(provider).then(function(result) {
//         // This gives you a Google Access Token. You can use it to access the Google API.
//         var token = result.credential.accessToken;
//         // The signed-in user info.
//         var user = result.user;
//         // ...
//       }).catch(function(error) {
//         // Handle Errors here.
//         var errorCode = error.code;
//         var errorMessage = error.message;
//         // The email of the user's account used.
//         var email = error.email;
//         // The firebase.auth.AuthCredential type that was used.
//         var credential = error.credential;
//         // ...
//       });
// }

NavHead(e){
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  }


render() {

    if (this.state.current=='signup') {
        return <Redirect to='/signup'/>;
    }

return (
    <div>
        <div>
                <Row style={{height:60}}>
                    <img style={{marginLeft:'49%'}} src={logo} alt="logo" height="55px" width="55px" />
                </Row>
                <Menu
                    theme="dark"
                    onClick={this.NavHead.bind(this)}
                    selectedKeys={[this.state.current]}
                    mode="horizontal"
                    style={{}}
                >
                
                    <Menu.Item key="signin">
                    <Icon type="lock" />Sign In
                    </Menu.Item>
                    <Menu.Item key="signup">
                    <Icon type="user-add" />Sign Up
                    </Menu.Item>
                </Menu>
        </div>
        <Col xs={{ span: 24 }} lg={{ span: 12 }} sm={{ span: 12 }} md={{ span: 12 }}>
            <img alt="login" src={image} style={{width:"100%"}} />
        </Col>
        <Col style={{ 
            background: '#ffffff',
            height:500,
            }} 
            xs={{ span: 24 }} lg={{ span: 12}} sm={{ span: 12 }} md={{ span: 12 }}>
            <Row type="flex" justify="center" style={{marginTop:40}}>

                <div>
                    <div style={{fontSize:17, color:"#616161"}}>ENTER YOUR CREDENTIALS</div>
                    
                    <LoginFormComp />
                    <Row>
                        <p style={{margin: 10, marginTop:0}}>
                            <Link to="/forgotpwd">
                                Forgot Password
                            </Link>
                        
                            <Link to="/signup" style={{marginLeft:60}}>
                                Sign Up
                            </Link>
                        </p>
                    </Row>

                    {/* <Row>
                        <center>or Login with</center>
                    </Row>
                    
                    <Row>
                    <Button
                        style={{width:250, marginBottom:20, marginTop:20, backgroundColor:"#d34836"}}
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                        //onClick={this.google.bind(this)}
                        >
                            Google
                        </Button>
                    </Row>
                    <Row>
                        <Button
                        style={{width:250, backgroundColor:'#3B5998'}}
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                        //onClick={this.facebook.bind(this)}
                        >
                            Facebook
                        </Button> 

                    </Row>*/}
                </div>
            </Row>
        </Col>
</div>
);
}
}
  
export default Login;

