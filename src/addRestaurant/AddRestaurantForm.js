import React, {Component} from 'react';
import { message, Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
var axios = require('axios');


class AddRestaurantForm extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    message:false,
    loading:false,
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);

        this.setState({
            loading:true
        });

        axios.post("https://api.marvolez.com/addedRestaurant",{
          name: values.name,
          address: values.address,
          contactPerson: values.contactPerson,
          city: values.city,
          state: values.state,
          phone: values.phone,
          pincode:values.pincode,
          email: values.email,
        })
        .then((response) => {
    
          if (response.data===true) {
            message.success('Your restaurant has been added for validation');
          }else{
            message.error('Your restaurant could not be added');
          }

          this.setState({
            loading:false
          });
        }).catch(err => {
        this.setState({ message: err.message });
        });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { autoCompleteResult } = this.state;
    let msg = null;
    if (this.state.message===false) {
        msg = <div></div>;
      } else {
        msg = <center style={{backgroundColor:'#f2f2f2', padding:10, marginBottom:10}}>{this.state.message}</center>;
      }

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '+1',
    })(
      <Select style={{ width: 70 }}>
        <Option value="+1">+1</Option>
      </Select>
    );

    const websiteOptions = autoCompleteResult.map(website => (
      <AutoCompleteOption key={website}>{website}</AutoCompleteOption>
    ));

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem
          {...formItemLayout}
          label="Restaurants Name"
        >
          {getFieldDecorator('RestaurantName', {
            rules: [{
              required: true, message: 'Please input your restaurants name',
            }],
          })(
            <Input />
          )}
        </FormItem>
        
        <FormItem
          {...formItemLayout}
          label="Address"
        >
          {getFieldDecorator('Address', {
            rules: [{
              required: true, message: 'Please input your restaurants address!',
            }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Contact Person"
        >
          {getFieldDecorator('contactPerson', {
            rules: [{
              required: true, message: 'Please enter your name!',
            }],
          })(
            <Input />
          )}
        </FormItem>
        
        <FormItem
          {...formItemLayout}
          label={(
            <span>
              City
            </span>
          )}
        >
          {getFieldDecorator('city', {
            rules: [{ required: true, message: 'Please input the city', whitespace: true }],
          })(
            <Input />
          )}
        </FormItem>
        
        <FormItem
          {...formItemLayout}
          label={(
            <span>
              State
            </span>
          )}
        >
          {getFieldDecorator('state', {
            rules: [{ required: true, message: 'Please input the state code', whitespace: true }],
          })(
            <Input />
          )}
        </FormItem>
        
        <FormItem
          {...formItemLayout}
          label={(
            <span>
              Zipcode
            </span>
          )}
        >
          {getFieldDecorator('zipcode', {
            rules: [{ required: true, message: 'Please input the zipcode', whitespace: false }],
          })(
            <Input />
          )}
        </FormItem>
        

        <FormItem
          {...formItemLayout}
          label="Phone Number"
        >
          {getFieldDecorator('phone', {
            rules: [{ required: true, message: 'Please input your phone number!' }],
          })(
            <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
          )}
        </FormItem>

        {msg}

        <FormItem {...tailFormItemLayout}>
          <Button 
          type="primary" 
          loading={this.state.loading}
          htmlType="submit">Add Restaurant</Button>
        </FormItem>
      </Form>
    );
  }
} 

const AddRestaurantFormComp = Form.create()(AddRestaurantForm);
  
export default AddRestaurantFormComp;