import React, {Component} from 'react';
import axios from 'axios';
import { message, Modal, Button, Row, Col, Input, Form, LocaleProvider } from 'antd';
import {connect} from 'react-redux';
import AddRestaurantFormComp from './AddRestaurantForm';
import enUS from 'antd/lib/locale-provider/en_US';
const FormItem = Form.Item;


class AddRestaurant extends Component {

  constructor(props) {
    super(props);
    this.state = {
        RestaurantName:'',
        title: '',
        visible: false,
        confirmLoading: false,
        category:'',
        restaurants:[],
        name: '',
        address: '',
        contactPerson: '',
        city: '',
        state: '',
        phone: '',
        pincode:'',
        email: '',
        confirmDirty: false,
        autoCompleteResult: [],
        message:false,
        loading:false,
    }
    
  } 

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  handleOk = () => {
    axios.post("https://api.marvolez.com/addedRestaurant",{
        name: this.state.name,
        address: this.state.address,
        contactPerson: this.state.contactPerson,
        city: this.state.city,
        state: this.state.state,
        phone: this.state.phone,
        pincode:this.state.pincode,
        email: this.state.email,
    })
    .then((response) => {

      if (response.data===true) {
        message.success('Your restaurant has been added for validation');
      }else{
        message.error('Your restaurant could not be added');
      }

      this.setState({
        visible: false,
        name: '',
        address: '',
        contactPerson: '',
        city: '',
        state: '',
        phone: '',
        pincode:'',
        email: ''
      });
    });
  }

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  }

  handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
      name: '',
      address: '',
      contactPerson: '',
      city: '',
      state: '',
      phone: '',
      pincode:'',
      email: ''
    });
  }


  addRestaurantFunc = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);

        axios.post("https://api.marvolez.com/addUserDetails",{
          name: values.name,
          address: values.address,
          contactPerson: values.contactPerson,
          city: values.city,
          state: values.state,
          phone: values.phone,
          pincode:values.pincode,
          email: values.email,
        })
        .then((response) => {
    
        if (response.data===true) {
            this.setState({
                message:'Registration Successful. Please login.',
                email: '',
                firstname: '',
                lastname: '',
                phone: '',
                address: '',
                status: '',
                password:'',
                type: '',
                loading:false
            });
        }
        else this.setState({message: Error, loading:false });
        }).catch(err => {
        this.setState({ message: err.message });
        });
      }
    });
  }



  render() {
    const { autoCompleteResult } = this.state;
    const { visible, confirmLoading } = this.state;
    
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
      },
    };
    
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    return(
      <div>
        <LocaleProvider locale={enUS}>
        <div>
          <Button type="primary" style={{marginBottom:20}} onClick={this.showModal}>Click Here</Button>
          <Modal title="Add Your Restaurant"
            visible={visible}
            onOk={this.handleOk}
            onSubmit={this.handleInputChange.bind(this)}
            confirmLoading={confirmLoading}
            onCancel={this.handleCancel}
            footer={null}
          >
            <AddRestaurantFormComp />
          </Modal>
        </div>
        </LocaleProvider>
      </div>
    );
  }
}


function mapStateToProps(state){
  return{
    user: state.reducerUser,
  };
}

function mapDispatchToProps(dispatch){
  return{
    setEmailType:(emailtype) => {
      dispatch({
        type:"LOGGED_IN",
        payload:emailtype
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRestaurant);

// export default AddRestaurant;