import React, {Component} from 'react';
import { Menu, Icon, Row, Col, Form,  Input, Button,  Card, } from 'antd';
//import Navbar from './navbar';
import {connect} from 'react-redux';
import back from './images/img.jpg'
import Head from './header';
import RegistrationFormComp from './RegistrationForm';
import logo from './images/marvolez.png';
import {
    Redirect, Link
  } from 'react-router-dom';

const FormItem = Form.Item;

class Registration extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            firstname: '',
            lastname: '',
            phone: '',
            address: '',
            status: 'inactive',
            password:'',
            type: 'client',
            current:'signup'
        };
    }

    NavHead(e){
        console.log('click ', e);
        this.setState({
          current: e.key,
        });
      }


    render(){
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 10 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 14 },
            },
        };

        if (this.state.current=='signin') {
            return <Redirect to='/'/>;
        }

        return(
            <div>
               <div>
                    <Row style={{height:60}}>
                        <img style={{marginLeft:'49%'}} src={logo} alt="logo" height="55px" width="55px" />
                    </Row>
                    <Menu
                        theme="dark"
                        onClick={this.NavHead.bind(this)}
                        selectedKeys={[this.state.current]}
                        mode="horizontal"
                        style={{}}
                    >
                    
                        <Menu.Item key="signin">
                        <Icon type="lock" />Sign In
                        </Menu.Item>
                        <Menu.Item key="signup">
                        <Icon type="user-add" />Sign Up
                        </Menu.Item>
                    </Menu>
                </div>

                <div>
                <Row>
                    <Col style={{backgroundImage: 'url(' +back+ ')', color:'#fafafa'}} xs={{ span: 24 }} lg={{ span: 12 }} sm={{ span: 12 }} md={{ span: 12 }}>
                        <div style={{fontSize: 15, paddingBottom:40,paddingTop:40, backgroundColor:'#000000', opacity:0.7}}>
                <center>
                <Row style={{fontSize:25, fontWeight: 'bold',}}>
                    OWN A RESTAURANT?
                </Row>
                </center>
                <br />
                <Row>
                    <Col style={{fontSize:20}} xs={{span:16, offset:4}}>
                    Welcome to Marvolez, an innovative way to showcase your restaurant. Living in the moment is what truly matters to your Patrons. Marvolez gives this very experience around your location. In our portal you can use the free service we offer to post specifically designed Marvols- ‘Virtual Billboards’ which emulate your presence in the surrounding environment. To drive new business and increased revenue, all you need to do is simply enter your offerings such as ‘special of the day’ or ‘deal of the week’ through our portal.
                    </Col>
                </Row>
                </div>


                    </Col>
                    
                    <Col xs={{ span: 24 }} lg={{ span: 12}} sm={{ span: 12 }} md={{ span: 12 }}>
                        <center>
                            <div style={{fontSize:17, color:"#616161", marginTop:50, marginBottom:30}}>REGISTER YOURSELF WITH US</div>
                        </center>
                        <RegistrationFormComp />
                    </Col>
                </Row>
                </div>


                <Row>

                <div style={{backgroundColor:"#000000", marginTop: 20, padding:40}}>
                    <center>
                    <div style={{fontSize:26, padding: 20, fontWeight: 'bold', color:"#ffffff"}}>
                        STEPS TO CLAIM YOUR RESTAURANT
                    </div>
                    </center>
                    <div>
                        <Row>
                            <Col xs={{ span: 24}} lg={{ span: 7, offset: 1  }} sm={{ span: 7, offset: 1 }} md={{ span: 7, offset: 1  }}>
                                <Card style={{height:230}}>
                                    <center><Icon type="login" style={{ fontSize: 25 }}/></center> 
                                    <br /><br />
                                    <center><div style={{fontSize: 20, fontWeight: 'bold'}}>STEP 1</div>
                                    <p style={{fontSize: 15}}>
                                    Fill out the form above and register yourself with us.
                                    </p>
                                    </center> 
                                </Card>
                            </Col>

                            <Col xs={{ span: 24}} lg={{ span: 7, offset: 1  }} sm={{ span: 7, offset: 1 }} md={{ span: 7, offset: 1  }}>
                                <Card style={{height:230}}>
                                    <center><Icon type="code" style={{ fontSize: 25 }}/></center> 
                                    <br /><br />
                                    <center><div style={{fontSize: 20, fontWeight: 'bold'}}>STEP 2</div>
                                    <p style={{fontSize: 15}}>
                                    Enter the zip code your business location is in
                                    </p>
                                    </center>
                                </Card>
                            </Col>

                            <Col xs={{ span: 24}} lg={{ span: 7, offset: 1  }} sm={{ span: 7, offset: 1 }} md={{ span: 7, offset: 1  }}>
                                <Card style={{height:230}}>
                                    <center><Icon type="search" style={{ fontSize: 25 }}/></center> 
                                    <br /><br />
                                    <center><div style={{fontSize: 20, fontWeight: 'bold'}}>STEP 3</div>
                                    <p style={{fontSize: 15}}>
                                    Search within our database and select claim
                                    </p>
                                    </center>
                                </Card>
                            </Col>

                            <Col xs={{ span: 24}} lg={{ span: 7, offset: 1  }} sm={{ span: 7, offset: 1 }} md={{ span: 7, offset: 1  }} style={{marginTop: 20}}>
                                <Card style={{height:260}}>
                                    <center><Icon type="mobile" style={{ fontSize: 25 }} /> </center> 
                                    <br /><br />
                                    <center><div style={{fontSize: 20, fontWeight: 'bold'}}>STEP 4</div>
                                    <p style={{fontSize: 15}}>
                                    You will receive an automated phone call with a pin number, this is used to validate your restaurant
                                    </p>
                                    </center>
                                </Card>
                            </Col>

                            <Col xs={{ span: 24}} lg={{ span: 7, offset: 1  }} sm={{ span: 7, offset: 1 }} md={{ span: 7, offset: 1  }} style={{marginTop: 20}}>
                                <Card style={{height:260}}>
                                    <center><Icon type="check-circle" style={{ fontSize: 25 }} /> </center> 
                                    <br /><br />
                                    <center><div style={{fontSize: 20, fontWeight: 'bold'}}>STEP 5</div>
                                    <p style={{fontSize: 15}}>
                                    Once you have been validated, you can type your specials and appeal to your customers in various ways
                                    </p>
                                    </center>
                                </Card>
                            </Col>

                            <Col xs={{ span: 24}} lg={{ span: 7, offset: 1  }} sm={{ span: 7, offset: 1 }} md={{ span: 7, offset: 1  }} style={{marginTop: 20}}>
                                <Card style={{height:260}}>
                                    <center><Icon type="plus-circle-o" style={{ fontSize: 25 }} /> </center> 
                                    <br /><br />
                                    <center><div style={{fontSize: 20, fontWeight: 'bold'}}>STEP 6</div>
                                    <p style={{fontSize: 15}}>
                                    If you cannot find your restaurant, please fill out the "Add Restaurant" form and we’ll manually validate you.
                                    </p>
                                    </center>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>

                </Row>
            </div>
        );
    }
}


function mapStateToProps(state){
    return{
      user: state.reducerUser,
    };
  }
  
export default connect(mapStateToProps)(Registration);