import React, {Component} from 'react'
import {
  BrowserRouter as Router,
  Route } from 'react-router-dom';

import Login from './login';
import Registration from './Registration';
import Dashboard from './dashboard';
import Forgotpwd from './forgotpwd';
import Logout from './logout';
import Search from './search';
import Settings from './settings/settings';



export default class Routes extends Component {

  render() {
    return(
      <Router>
        <div>   
          <Route exact path="/" component={Login}/>
          <Route path="/signup" component={Registration}/>
          <Route path="/forgotpwd" component={Forgotpwd}/>
          <Route path="/dashboard" component={Dashboard}/>
          <Route path="/logout" component={Logout}/>
          <Route path="/search" component={Search}/>
          <Route path="/settings" component={Settings}/>
        </div>
      </Router>
    );
  }
}